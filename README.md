# README #



### What is this repository for? ###

This repo will hold an application that will provide an access token to be used in the resource server
to gain access to different parts of the resource owner.

A client will be registered whithin this server and will use a client_id and a client secret to get an access token

### How to request manually a token ###

To request for a new access token manually use the next command:

curl -XPOST -k foo:foosecret@localhost:9000/auth/oauth/token    -d grant_type=password -d client_id=foo -d client_secret=foosecret -d redirect_uri=http://www.google.es -d username=piritz -d password=piritzSecret

-clientId:foo. This is the client application id. 
-clientSecret:foosecret. this is the client secret.
-username: this is the application security username
-password: this is the application security password

This is for research purposes, final authentication server application should not use application security and just use the client registered id and secret